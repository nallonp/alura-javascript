const botaoAdicionar = document.querySelector("#buscar-pacientes");
botaoAdicionar.addEventListener("click", function () {
    const xhr = new XMLHttpRequest();
    const erroAjax = document.querySelector("#erro-ajax");
    xhr.open("GET", "http://api-pacientes.herokuapp.com/pacientes");
    xhr.addEventListener("load", function () {
        if (xhr.status !== 200) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            erroAjax.classList.remove("invisivel");
            return;
        }
        const pacientes = JSON.parse(xhr.responseText);
        pacientes.forEach(adicionaPacienteNaTabela);
        erroAjax.classList.add("invisivel");
    });
    xhr.send();
});