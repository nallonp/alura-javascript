// Função anônima.
var botaoAdicionarPaciente = document.querySelector('#adicionar-paciente');

function exibeMensagensDeErro(erros) {
    let ul = document.querySelector("#mensagens-erro");
    ul.innerHTML = "";
    erros.forEach(e => {
        let li = document.createElement("li");
        li.textContent = e;
        ul.appendChild(li);
    })
}

function adicionaPacienteNaTabela(paciente) {
    const pacienteTr = montaTr(paciente);
    const tabela = document.querySelector('#tabela-pacientes');
    tabela.appendChild(pacienteTr);
}

botaoAdicionarPaciente.addEventListener('click', function (event) {
    event.preventDefault();
    const form = document.querySelector('#form-adiciona');
    const paciente = obtemPacienteDoFormulario(form);
    const erros = validaPaciente(paciente);
    if (erros.length > 0) {
        exibeMensagensDeErro(erros);
        return;
    }
    adicionaPacienteNaTabela(paciente);
    form.reset();
    let ul = document.querySelector("#mensagens-erro");
    ul.innerHTML = "";
})

function obtemPacienteDoFormulario(form) {
    return {
        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calculaIMC(form.peso.value, form.altura.value)
    };
}

function montaTr(paciente) {
    const pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");
    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));
    return pacienteTr;
}

function montaTd(dado, classe) {
    const td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);
    return td;
}

function validaPaciente(paciente) {
    var erros = [];
    if (paciente.nome.length === 0) erros.push("Nome do paciente não pode estar em branco.");
    if (paciente.peso.length === 0) erros.push("Peso não pode estar em branco.");
    if (paciente.altura.length === 0) erros.push("Altura não pode estar em branco.");
    if (paciente.gordura.length === 0) erros.push("Percentual de gordura não pode estar em branco.");
    if (!validaPeso(paciente.peso)) erros.push("Peso inválido!");
    if (!validaAltura(paciente.altura)) erros.push("Altura inválida!");
    return erros;
}