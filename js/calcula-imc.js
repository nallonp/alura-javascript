//console.log('Fui carregado de um arquivo externo.');
var titulo = document.querySelector('.titulo');
titulo.textContent = 'Aparecida Nutricionista.';

//var paciente = document.querySelector('#primeiro-paciente');
var pacientes = document.querySelectorAll('.paciente');

for (let index = 0; index < pacientes.length; index++) {
    const paciente = pacientes[index];
    var tdAltura = paciente.querySelector('.info-altura');
    var tdPeso = paciente.querySelector('.info-peso');
    var tdImc = paciente.querySelector('.info-imc');

    var altura = tdAltura.textContent;
    var peso = tdPeso.textContent;

    var pesoEhValido = validaPeso(peso);
    var alturaEhValida = validaAltura(altura);

    if (!pesoEhValido) {
        tdPeso.textContent = 'Peso inválido!';
        pesoEhValido = false;
        paciente.classList.add('paciente-invalido');
    }

    if (!alturaEhValida) {
        //console.log('Altura inválida!');
        tdAltura.textContent = 'Altura inválida!';
        alturaEhValida = false;
        paciente.classList.add('paciente-invalido');
    }

    if (alturaEhValida && pesoEhValido) {
        tdImc.textContent = calculaIMC(peso, altura);
    } else {
        tdImc.textContent = 'Altura e/ou peso inválidos!'
    }
}

/* 
Função.
titulo.addEventListener('click', function mostraMensagem() {
    console.log('fui clicado');
}); 
*/

function calculaIMC(peso, altura) {
    return (peso / (altura ** 2)).toFixed(2);
}

function validaPeso(peso) {
    return peso >= 0 && peso < 1000;
}

function validaAltura(altura) {
    return altura >= 0 && altura < 3.0;
}